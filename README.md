# Syntax

```
# Commentary
(Finite|Pushdown|Turing) <name>
    -> <state> <state> * [! <state>] [<state>...];
    
    # Finite
     <state> <condition> <state>
    [<state> <condition> <state>...]
    # Pushdown
     <state> <condition> <pop> <push> <state>
    [<state> <condition> <pop> <push> <state>...]
    #Turing
     <state> <read> <write> (r|l|s) <state>
    [<state> <read> <write> (r|l|s) <state>...];
    
    [<input>...];
```

```-> <state>``` Initial state. 

```<state> *``` Final state.

```! <state>``` State refers to a Turing machine.

```_``` = ε or empty symbol.

[example](exemplo.m)

# Building
```git clone --recurse-submodules https://evandro-crr@gitlab.com/evandro-crr/automata.git```

``` cd automata && make ```

# Usage 

``` ./automata <input file> [-p] ```
