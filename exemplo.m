# L(m1) = (a^n | n is pair)
Finite m1 
  ->q0*;

  q0 a q1
  q1 a q0;

  _ a aa aaa aaaa;

# L(m2) = (a^nb^n | n >= 0)
Pushdown m2 
  ->q0 q2*;

  q0 a _ a q0
  q0 _ _ _ q1
  q1 b a _ q1
  q1 _ $ _ q2;

  _ ab aabb ba aaab;

# L(m3) = (a^nb^nc^n | n >= 1)
Turing m3
  ->q0 q5*;   

  q0 A A r q0
  q0 a A r q1

  q0 B B r q0
  q0 C C r q0
  q0 _ _ s q5

  q1 a a r q1
  q1 B B r q1
  q1 b B r q2

  q2 b b r q2
  q2 C C r q2
  q2 c C r q3

  q3 c c r q3
  q3 _ _ l q4

  q4 a a l q4
  q4 A A l q4
  q4 b b l q4
  q4 B B l q4
  q4 c c l q4
  q4 C C l q4
  q4 _ _ r q0;

  _ abc aabbcc abcc aabbacc; 

# Divide input into two
Turing div
  ->q0 q7*;
  
  q0 A A r q0
  q0 B B l q4
  q0 a A r q1

  q1 a a r q1
  q1 B B r q1
  q1 _ _ l q2

  q2 B B l q2
  q2 a B l q3

  q3 a a l q3
  q3 A A l q3
  q3 _ _ r q0

  q4 A A l q4
  q4 _ _ r q5
  q5 A _ r q5
  q5 B a r q5
  q5 _ _ l q6
  q6 a a l q6
  q6 _ _ r q7;;

# L(m3) = (a^(2^n) | n >= 0)
Turing m3
  ->q0 !div q2*;

  q0 a a r q1
  q1 _ _ s q2
  q1 a a l div
  div a a s q0;
  a aa aaa aaaa aaaaa aaaaaaaa;

