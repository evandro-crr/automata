#include <iostream>
#include "automata.h"
#include "turing.h"
#include "parsing.cpp"
#include  "../cpp-readline/src/Console.hpp"  

void compute_all(Automatas &automatas) {
  for (auto automata : automatas) {
    bool name{true};
    for (auto input : automata.second.second) {
      if (name) {
        std::cout << automata.first << std::endl;
        name = false;
      }
      std::cout << "  " 
        << (automata.second.first->compute(input.c_str())? ":) " : ":( ")
        << input <<std::endl;
    }
    if (not name) std::cout << std::endl;
  }
}

void compute(Automata* automata, const std::vector<std::string> &inputs) {
  unsigned jump{1};
  bool trace{false};
  if (inputs.size() >= 2 and inputs[1] == "--trace") {
    trace = true; 
    jump++;
  }
  for (auto input : inputs) {
    if (jump) {
      jump--;
      continue;
    }
    bool result;
    bool nl{false};
    if (trace and dynamic_cast<Turing*>(automata) != nullptr) {
      result = not ((Turing*)automata)->compute(Tape{input}, true).empty();
      nl = true;
    } else {
      input = std::regex_replace(input, std::regex("_"), "");
      result = automata->compute(input.c_str());
    }
    std::cout << "  " << (result? ":) " : ":( ") << input;
    if (nl) std::cout << std::endl;
    std::cout << std::endl;
  }
}

void make_commad(Automatas &automatas, CppReadline::Console &console) {
  for (auto automata : automatas) {
    auto command = [=](const std::vector<std::string> &inputs){
      compute(automata.second.first, inputs);
      return 0u;
    };
    console.registerCommand(automata.first, command); 
  }
}

int main(const int argc, const char* argv[]) {

  auto automatas = parsing(argv[1]);
  if (argc > 2 and argv[2] == std::string{"-p"}) {
    std::cout << "Usage: <automata name> [--trace] <input> [<input>...]" << std::endl;
    CppReadline::Console console{">>> "};
    make_commad(automatas, console);
    int ret;
    console.registerCommand("all", [&](const std::vector<std::string>) {
      compute_all(automatas); 
      return 0u;
    });
    do {
      ret = console.readLine();
    } while (ret != CppReadline::Console::ReturnCode::Quit);
  } else compute_all(automatas);

  return 0;
}
