#pragma once 
#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <vector>
#include "automata.h"
#include "turing.h"

using Automatas = std::unordered_map<std::string,
                  std::pair<Automata*, std::vector<std::string>>>;

std::vector<std::string> tokenize(std::string &sfile) {

  sfile = std::regex_replace(sfile, std::regex("  "), " ");
  sfile = std::regex_replace(sfile, std::regex("  "), " ");
  std::regex token{"([;*_!#\\$]|\\w+|->|[^\\s]+)"};
  std::regex_iterator<std::string::iterator> rit{sfile.begin(), sfile.end(), token};
  std::regex_iterator<std::string::iterator> rend;  
  std::vector<std::string> tokens; 
  while (rit != rend) {
    tokens.push_back(rit->str());
    rit++;
  }

  return tokens;
}

bool valid(std::string state, std::regex token, std::string path,
           unsigned cline, std::string line, std::string messege) {
  bool result{true};
  if (not std::regex_match(state, token)) {
    std::cerr << path << ":" << cline << ": " << line << std::endl
              << "  '" << state << "' "<< messege << std::endl;
    result = false; 
  }
  return result;
}

Automatas parsing(const char path[]) {

  std::ifstream file{path};

  Automatas automatas;
  enum States {D, S, T, I, E} state{D};
  enum Types {F, P, TM} type;
  bool init,_init,_final, have_final,_link, _erro, erro;
  std::string name, line;
  unsigned cline{0}, nline, econt;
  Automata *automata;
  std::vector<std::string> inputs;
  std::regex rstate{"\\w+"}, rcondition{"\\w"},
             rdirection{"(r|l|s)"}, rstack{"(\\w*\\$\\w*|\\w+)"};
  
  while (std::getline(file, line)) {
    cline++;
    auto tokens = tokenize(line);
    for (auto it = tokens.begin(); it != tokens.end(); it++) {
      if (*it == "#") break; 
      switch (state) {
        case D:
          init = false;
          have_final = false;
          erro = false;
          if (*it == "Finite") {
            type = F;
            automata = new Finite();
          } else if (*it == "Pushdown") {
            type = P;
            automata = new Pushdown();
          } else if (*it == "Turing") {
            type = TM;
            automata = new Turing();
          } else {
            std::cerr << path << ":" << cline << ": " << line << std::endl << "  '" << *it
                      << "' unknow automata" << std::endl;
            erro = true;
            econt = 0;
            state = E;
            break;
          }
          it++;
          name = *it;
          nline = cline;
          state = S;
          break;

        case S:
          if (*it == ";") {
            state = T;
            if (not init) {
              std::cerr << path << ":" << nline << ": "
                        << name << " do not have a initial state" << std::endl;
              erro = true;
            }
            if (not have_final) {
              std::cerr << path << ":" << nline << ": "
                        << name << " do not have a final state" << std::endl;
              erro = true;
            }
            break;
          }

          _init = false;
          _link = false;
          if (init == false and *it == "->") {
            init = true;
            _init = true;
            it++;
          }
          if (*it == "!" and type == TM) {
            _link = true;
            it++;
          }
          _final = *(it+1) == "*";
          have_final = _final or have_final;

          automata->add_state(*it, _final);
          if (_init) automata->set_init(*it);
          if (_link) {
            if (dynamic_cast<Turing*>(automatas[*it].first) != nullptr) {
              ((Turing*)automata)->link_state(*it, (Turing*)automatas[*it].first);
            }  else {
              std::cerr << path << ":" << cline << ": " << line << std::endl
                        << "  '" << *it << "' is not a Turing machine" << std::endl;
              erro = true;
            }
          }
          if (_final) it++;
          break;

        case T:
          if (*it == ";") {
            state = I;
            inputs.clear();
            break;
          }
          
          _erro = false;

          _erro = _erro or not valid(*it, rstate, path, cline, line,
                                     "state name must be alphanumeric");

          switch (type) {
            case F:
              _erro = _erro or not valid(*(it+1), rcondition,
                                path, cline, line, "expecting one alphanumeric character");
              _erro = _erro or not valid(*(it+2), rstate, path, cline,
                                       line, "expecting a state");
              if (not _erro)
              ((Finite*)automata)->add_transition(*it, (*(it+1))[0], *(it+2));
              it += 2;
              break;
            case P:
              _erro = _erro or not valid(*(it+1), rcondition,
                                path, cline, line, "expecting one alphanumeric character");
              _erro=_erro or not valid(*(it+2), rstack, path, cline,line,
                                       "invalid stack simble");
              _erro=_erro or not valid(*(it+3), rstack, path, cline,line,
                                        "invalid stack simble");
              _erro = _erro or not valid(*(it+4), rstate, path, cline,
                                       line, "expecting a state");
              if (not _erro)
              ((Pushdown*)automata)->add_transition(*it, (*(it+1))[0],
                                                    *(it+2), *(it+3), *(it+4));
              it += 4;
              break;
            case TM:
              _erro = _erro or not valid(*(it+1), rcondition, path, cline,
                                       line, "expecting one alphanumeric character");
              _erro = _erro or not valid(*(it+2), rcondition, path, cline,
                                       line, "expecting one alphanumeric character");
              _erro = _erro or not valid(*(it+3), rdirection, path, cline,
                                       line, "direction must be r, l or s");
              if (not _erro)
              ((Turing*)automata)->add_transition(*it, (*(it+1))[0],
                                   (*(it+2))[0], make_direction((*(it+3))[0]), *(it+4));
              it += 4;
              break;
          }
          erro = erro or _erro;
          break;

        case I:
          if (*it == ";") {
            state = D;
            if (not erro) automatas[name] = std::make_pair(automata, inputs);
          } else if (valid(*it, rstate, path, cline, line,
                           "expecting a alphanumeric input")){
            inputs.push_back(std::regex_replace(*it, std::regex("_"), ""));
          }
          break;
        
        case E:
          if (*it == ";") {
            if (econt == 2) state = D;
            else econt++;
          }
          break;    
      }
    }
  }
  return automatas;
}

