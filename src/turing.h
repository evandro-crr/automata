#pragma once
#include <iostream>
#include "automata.h"

class Tape {
 public:
  enum Direction{L, S, R};

  Tape(std::string tape);
  Tape() {}
  
  void right(char write); 
  void left(char write); 
  void stay(char write);
  char read() const;
  bool empty() const;
  std::string getTape() const;

 private:
  std::string tape{""};
  unsigned head{0};
};

Tape::Direction make_direction(const char d);

class Turing :public Automata {
 public:
   void link_state(std::string &state, Turing* automata);
   void add_transition(const std::string &actual_state,
                       const char read,
                       const char write,
                       const Tape::Direction direction,
                       const std::string &next_state);
   bool compute(const char input[]) const;
   Tape compute(Tape tape, bool trace) const;

 private:
   std::unordered_map<unsigned, Turing*> blocks;
   std::unordered_map<unsigned,
                      std::unordered_map<char,
                      std::vector<std::tuple<char, Tape::Direction, unsigned>>>> transition;

};
