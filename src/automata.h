#pragma once
#include <map>
#include <regex>
#include <vector>
#include <string>
#include <sstream>
#include <utility>
#include <unordered_map>

class Automata {
 public:
  void add_state(const std::string &name, bool final_state);
  void set_init(const std::string &name);
  virtual bool compute(const char input[]) const = 0;

 protected:
  std::vector<bool> states;  
  std::unordered_map<std::string ,unsigned> names;
  unsigned init;
};

class Finite : public Automata {
 public:
  void add_transition(const std::string &actual_states,
                      const char condition,
                      const std::string &next_state);
  bool compute(const char input[]) const;

 private:
  void epsilon_transition(std::vector<unsigned> &actual_states) const;

  std::map<std::pair<unsigned, char>, std::vector<unsigned>> transition;

}; 

class Pushdown : public Automata {
 public:
  void add_transition(const std::string &actual_state,
                      const char condition,
                      const std::string &top_stack,
                      const std::string &push_stack,
                      const std::string &next_state);
  bool compute(const char input[]) const;
 
 private:
  void
  epsilon_transition(std::vector<std::pair<unsigned, std::string>> &actual_states) const;

  std::unordered_map<unsigned,
                     std::unordered_map<char,
                     std::unordered_map<std::string,
                     std::vector<std::pair<std::string, unsigned>>>>> transition;
};
