#include "automata.h"

void Automata::add_state(const std::string &name, bool final_state) {
  if (name == "") throw std::logic_error{"undefined state"};
  states.push_back(final_state);
  names[name] = states.size() -1;
}

void Automata::set_init(const std::string &name) {
  try {
    init = names.at(name);
  } catch (const std::out_of_range) {
    throw std::out_of_range{name + " : undefined initial state"};
  }
}

void Finite::add_transition(const std::string &actual_state,
                             const char condition,
                             const std::string &next_state) {
  try {
    transition[std::make_pair(names.at(actual_state), condition == '_'? 0 : condition)]
              .push_back(names.at(next_state));
  } catch (const std::out_of_range) {
    try { names.at(actual_state); }
    catch (const std::out_of_range) { add_state(actual_state, false); }
    try { names.at(next_state); }
    catch (const std::out_of_range) { add_state(next_state, false); }
    add_transition(actual_state, condition, next_state);
  }
}

void Finite::epsilon_transition(std::vector<unsigned> &actual_states) const {
  for (auto i = 0; i < actual_states.size(); i++) {
    try {
      auto result = transition.at(std::make_pair(actual_states[i], 0));
      actual_states.insert(actual_states.end(), result.begin(), result.end());
    } catch (const std::out_of_range) {}
  }  
}

bool Finite::compute(const char input[]) const {
  std::vector<unsigned> actual_states{init}; 

  for (auto i = 0; input[i]; i++) {
    
    epsilon_transition(actual_states);

    std::vector<unsigned> next_states;

    for (auto state : actual_states) {
      try {
        auto result = transition.at(std::make_pair(state, input[i]));
        next_states.insert(next_states.end(), result.begin(), result.end());
        } catch (const std::out_of_range) {}
    }
    
    if (next_states.empty()) return false;
    
    actual_states = next_states;
  }

  epsilon_transition(actual_states);

  for (auto state : actual_states) {
    if (states[state]) return true;
  }

  return false;
}

void Pushdown::add_transition(const std::string &actual_state,
                              const char condition,
                              const std::string &top_stack,
                              const std::string &push_stack,
                              const std::string &next_state) {
  try {
    transition[names.at(actual_state)]
              [condition == '_'? 0 : condition]
              [top_stack == "_"? "" : top_stack]
              .push_back(std::make_pair(push_stack == "_"? "" : push_stack,
                                        names.at(next_state)));
  } catch (const std::out_of_range) {
    try { names.at(actual_state); }
    catch (const std::out_of_range) { add_state(actual_state, false); }
    try { names.at(next_state); }
    catch (const std::out_of_range) { add_state(next_state, false); }
    add_transition(actual_state, condition, top_stack, push_stack, next_state);
  }
}

void Pushdown::epsilon_transition(std::vector<std::pair<unsigned, std::string>>
                                  &actual_states) const {
  for (auto i = 0; i < actual_states.size(); i++) {
    try {
      auto result = transition.at(actual_states[i].first).at(0); 
      for (auto it = result.begin(); it != result.end(); it++) {
        if(it->first == actual_states[i]
                        .second.substr(actual_states[i].second.size()-it->first.size(),
                                       it->first.size())) {
          for (auto next_state : it->second) {
            actual_states.push_back(std::make_pair(next_state.second,
                                    actual_states[i].second.substr(0,
                                    actual_states[i].second.size()-it->first.size())
                                    +next_state.first)); 
          }
        }
      }
    } catch (const std::out_of_range) {}
  }
}

bool Pushdown::compute(const char input[]) const {
  std::vector<std::pair<unsigned, std::string>> actual_states{std::make_pair(init, "$")}; 
  
  for (auto i = 0; input[i]; i++) {
    epsilon_transition(actual_states);

    std::vector<std::pair<unsigned, std::string>> next_states; 

//transition[actual_state][input][top_stack]{(push_stack, next_state)...}
    for (auto state : actual_states) {
      try {
        auto result = transition.at(state.first).at(input[i]); 
        for (auto it : result) {
          if(it.first == state.second.substr(state.second.size()-it.first.size(),
                                              it.first.size())) {
            for (auto next_state : it.second) {
              next_states.push_back(std::make_pair(next_state.second,
                                 state.second.substr(0,
                                 state.second.size()-it.first.size())+next_state.first)); 
            }
          }
        }
      } catch (const std::out_of_range) {} 
    }

    if (next_states.empty()) return false;

    actual_states = next_states;
  }

  epsilon_transition(actual_states);

  for (auto state : actual_states) {
    if (states[state.first]) return true;
  }

  return false;
}

