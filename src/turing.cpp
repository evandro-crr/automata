#include "turing.h"

Tape::Tape(std::string tape) : tape{tape} {}

void Tape::right(char write) {
  tape[head] = write;
  if (head == tape.size()-1) {
    tape += "_";
  }
  head++;    
}

void Tape::left(char write) {
  tape[head] = write;
  if (head == 0) {
    tape = "_" + tape;  
  } else {
    head--;
  }
}

void Tape::stay(char write) {
  tape[head] = write;
}

char Tape::read() const {
  return tape[head];
}

bool Tape::empty() const {
  return tape == "";
}

std::string Tape::getTape() const {
  std::string _tape = "  " + tape + "\n  ";
  for (auto i = 0; i < head; i++) _tape += " ";
  _tape += "^";
  return _tape;
}

Tape::Direction make_direction(const char d) {
  if (d == 'r') return Tape::R;
  else if (d == 'l') return Tape::L;
  else if (d == 's') return Tape::S;
  else throw std::logic_error{d + " : undefined direction"};
}

void Turing::link_state(std::string &state, Turing* automata) {
  try {
   blocks[names.at(state)] = automata;
  } catch (const std::out_of_range) {
    throw std::out_of_range{"unknow turing machine"};
  }
}

void Turing::add_transition(const std::string &actual_state,
                    const char read,
                    const char write,
                    const Tape::Direction direction,
                    const std::string &next_state) {
  try {
    transition[names.at(actual_state)][read].push_back(
              std::make_tuple(write, direction, names.at(next_state)));
  } catch (const std::out_of_range) {
    try { names.at(actual_state); }
    catch (const std::out_of_range) { add_state(actual_state, false); }
    try { names.at(next_state); }
    catch (const std::out_of_range) { add_state(next_state, false); }
    add_transition(actual_state, read, write, direction, next_state);
  }
}

Tape Turing::compute(const Tape tape, bool trace) const {

  std::vector<std::pair<unsigned, Tape>> actual_states;
  try {
    auto begin_tape = blocks.at(init)->compute(tape, trace);
    if (not begin_tape.empty()) actual_states.push_back(std::make_pair(init, begin_tape));
  } catch (const std::out_of_range) {
    actual_states.push_back(std::make_pair(init, tape));
  }

  while(not actual_states.empty()) {
    std::vector<std::pair<unsigned, Tape>> next_states;
    for (auto actual_state : actual_states) {
      if (trace) {
        std::cout << actual_state.second.getTape() << std::endl;
      }
      try {
        auto tuples = transition.at(actual_state.first).at(actual_state.second.read()); 
        for (auto tuple : tuples) {
          auto new_tape = actual_state.second;
          auto direction = std::get<1>(tuple);
          switch (direction) {
            case Tape::L:
              new_tape.left(std::get<0>(tuple));
              break;
            case Tape::S:
              new_tape.stay(std::get<0>(tuple));
              break;
            case Tape::R:
              new_tape.right(std::get<0>(tuple));
              break;
          }
          try {
            new_tape = blocks.at(std::get<2>(tuple))->compute(new_tape, trace);
            if (new_tape.empty()) continue;
          } catch (const std::out_of_range) {}
          next_states.push_back(std::make_pair(std::get<2>(tuple), new_tape));
        }
      } catch (const std::out_of_range) {
        if (states[actual_state.first]) {
          return actual_state.second;
        }
      }
    } 
    actual_states = next_states;
  }
  return Tape{};
}

bool Turing::compute(const char input[]) const {
  auto tape = compute(Tape{input}, false);
  return not tape.empty();

}
